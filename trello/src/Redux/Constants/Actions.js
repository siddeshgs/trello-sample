import { BOARD, CARD, CHECKLIST, ITEM } from "../Constants/index";

export const fetchBoard = () => ({
  type: BOARD.LOAD
});

export const fetchCard = id => ({
  type: CARD.LOAD,
  payload: { id }
});

export const addCard = (id, input) => ({
  type: CARD.LOAD_ADD_CARD,
  payload: {
    id,
    input
  }
});

export const fetchCardChecklist = id => ({
  type: CARD.LOAD_CARD_CHECKLIST,
  payload: { id }
});

export const addChecklist = (id, name) => ({
  type: CHECKLIST.LOAD_ADD_CHECKLIST,
  payload: { id, name }
});

export const addItemData = data => ({
  type: ITEM.LOAD_ADD_ITEM_DATA,
  payload: { data }
});

export const addChecklistItem = (id, data) => ({
  type: ITEM.LOAD_ADD_CHECKLIST_ITEM,
  payload: { id, data }
});

export const putChecklistStatus = (
  cardID,
  itemId,
  checklistId,
  name,
  state
) => ({
  type: CHECKLIST.LOAD_PUT_CHECKLIST_STATUS,
  payload: { cardID, itemId, checklistId, name, state }
});

export const removeCard = id => ({
  type: CARD.LOAD_REMOVE_CARD,
  payload: { id }
});

export const removeChecklist = (checkListId, cardId) => ({
  type: CHECKLIST.LOAD_REMOVE_CHECKLIST,
  payload: { checkListId, cardId }
});

export const removeItem = (itemId, cardId) => ({
  type: ITEM.LOAD_REMOVE_ITEM,
  payload: { itemId, cardId }
});
