import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchCardChecklist } from "../../Redux/Constants/Actions";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import TextField from "@material-ui/core/TextField";
import CardHeader from "@material-ui/core/CardHeader";
import { addChecklist } from "../../Redux/Constants/Actions";
import { addItemData } from "../../Redux/Constants/Actions";
import { addChecklistItem } from "../../Redux/Constants/Actions";
import {
  putChecklistStatus,
  removeChecklist,
  removeItem
} from "../../Redux/Constants/Actions";

class Checklist extends Component {
  state = {
    check: false,
    showChecklist: false,
    checklistInput: "",
    showItem: true,
    itemInput: ""
  };

  componentWillMount = () => {
    this.props.fetchCardChecklist(this.props.match.params.cardId);
  };

  handleChange = e => {
    const input = e.target.id;
    // console.log(input);
    this.setState({
      input
    });
  };

  toggle = (val, id) => {
    if (val && this.state.itemInput !== "") {
      this.props.addChecklistItem(id, this.state.itemInput);
      return false;
    }
    return false;
  };

  changeItem = e => {
    // console.log(e.target.value);
    // console.log(this.props.lists.showItem);
    this.setState({
      itemInput: e.target.value
    });
  };

  addItem(id) {
    // console.log(id);
    this.setState({
      showItem:
        this.state.showItem === true
          ? this.toggle(this.state.showItem, id)
          : true
    });
  }
  clickChange = (cardID, itemId, checklistId, name, state) => {
    this.props.putChecklistStatus(cardID, itemId, checklistId, name, state);
  };

  removeItem = (itemId, cardId) => {
    this.props.removeItem(itemId, cardId);
  };
  removeChecklist = (checkListId, cardId) => {
    this.props.removeChecklist(checkListId, cardId);
  };

  render() {
    const data = this.props.lists.checklistdata;
    // console.log(this.props.lists.Checkbox);
    const cardId = this.props.match.params.cardId;
    // console.log(this.props.match.params);
    // console.log(this.props.lists.checklistdata);
    return data === undefined ? (
      <div>loading...</div>
    ) : (
      <Card className="checklists" style={{ textAlign: "center" }}>
        <CardHeader
          title={this.props.match.params.pathParam}
          subheader="checklists"
        />
        {data.map(checklist => {
          return (
            <Card
              key={checklist.id}
              style={{ marginBottom: "5%", width: "50%", marginLeft: "25%" }}
            >
              <Paper
                elevation={1}
                style={{
                  textAlign: "center",
                  padding: "1em",
                  fontWeight: "600",
                  color: "white",
                  backgroundColor: "black",
                  opacity: "0.8",
                  display: "flex",
                  flexWrap: "wrap"
                }}
              >
                <h4 style={{ width: "50%", textAlignLast: "right" }}>
                  {checklist.name}
                </h4>
                <Button
                  style={{
                    color: "white",
                    width: "20%",
                    marginLeft: "28%"
                  }}
                  onClick={() => this.removeChecklist(checklist.id, cardId)}
                >
                  remove
                </Button>
              </Paper>
              {checklist.checkItems.map(item => {
                return (
                  <div
                    key={item.id}
                    style={{
                      display: "flex",
                      flexWrap: "wrap",
                      width: "100%",
                      margin: "1% 0"
                    }}
                  >
                    <Checkbox
                      id={item.id}
                      checked={item.state === "complete" ? true : false}
                      onChange={() =>
                        this.clickChange(
                          this.props.match.params.cardId,
                          item.id,
                          checklist.id,
                          item.name,
                          item.state === "complete" ? "incomplete" : "complete"
                        )
                      }
                      color="primary"
                      style={{ width: "14%" }}
                    />
                    <Typography
                      style={{
                        paddingTop: "1em",
                        width: "60%"
                      }}
                    >
                      {item.name}
                    </Typography>
                    <Button
                      style={{ width: "15%" }}
                      onClick={() => this.removeItem(item.id, cardId)}
                    >
                      remove
                    </Button>
                  </div>
                );
              })}
              {this.state.showItem && (
                <TextField
                  id={checklist.id}
                  placeholder="Add Item"
                  multiline
                  margin="normal"
                  style={{
                    padding: "1em",
                    fontSize: "1em",
                    fontWeight: "500",
                    width: "90%"
                  }}
                  onChange={this.changeItem}
                />
              )}
              <Divider />
              <Button
                id={checklist.id}
                onClick={() => this.addItem(checklist.id)}
              >
                Add Items
              </Button>
            </Card>
          );
        })}
        <Divider />
        {this.state.showChecklist && (
          <TextField
            id={this.props.match.params.listId}
            placeholder="Add Checklist"
            multiline
            margin="normal"
            style={{
              padding: "1em",
              fontSize: "1em",
              fontWeight: "500",
              width: "90%"
            }}
            onChange={this.changeChecklist}
          />
        )}
        <Button onClick={this.addChecklist} style={{ margin: "1em" }}>
          Add checklist
        </Button>
      </Card>
    );
  }
  changeChecklist = e => {
    this.setState({
      checklistInput: e.target.value
    });
  };

  putChecklist = val => {
    if (val && this.state.checklistInput !== "") {
      this.props.addChecklist(
        this.props.match.params.cardId,
        this.state.checklistInput
      );
      return false;
    }
    return false;
  };

  addChecklist = () => {
    this.setState({
      showChecklist:
        this.state.showChecklist === true
          ? this.putChecklist(this.state.showChecklist)
          : true
    });
  };
}

const mapStateToProps = state => {
  return {
    lists: state.Reducer
  };
};
const mapDispatchToProps = dispatch => {
  return {
    fetchCardChecklist: id => dispatch(fetchCardChecklist(id)),
    addChecklist: (id, name) => dispatch(addChecklist(id, name)),
    addItemData: ary => dispatch(addItemData(ary)),
    addChecklistItem: (id, name) => dispatch(addChecklistItem(id, name)),
    putChecklistStatus: (cardID, itemId, checklistId, name, state) =>
      dispatch(putChecklistStatus(cardID, itemId, checklistId, name, state)),
    removeChecklist: (checkListId, cardId) =>
      dispatch(removeChecklist(checkListId, cardId)),
    removeItem: (itemId, cardId) => dispatch(removeItem(itemId, cardId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Checklist);
