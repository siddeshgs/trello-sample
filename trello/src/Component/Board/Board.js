import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchBoard } from "../../Redux/Constants/Actions";
import Lists from "../List/Lists";

class Board extends Component {
  componentDidMount = () => {
    this.props.fetchBoard();
    const board = this.props;
    if (board.BoardData !== undefined) {
      this.props.history.push(`/trello/board/${board.BoardData.id}`);
    }
  };
  render() {
    const board = this.props;
    // console.log(board);

    return board.BoardData ? (
      <div className="board" style={{ margin: "2em 0" }}>
        <Lists />
      </div>
    ) : (
      <h4>loading.....</h4>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return { fetchBoard: () => dispatch(fetchBoard()) };
};
const mapStateToProps = state => {
  return {
    BoardData: state.Reducer.data
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Board);
