import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { connect } from "react-redux";
import { fetchCard } from "../../Redux/Constants/Actions";
import { addCard } from "../../Redux/Constants/Actions";
import { NavLink } from "react-router-dom";
import { removeCard } from "../../Redux/Constants/Actions";

class Cards extends Component {
  state = {
    show: false,
    input: ""
    // redirect: false
  };

  toggle = val => {
    if (val && this.state.input !== "") {
      this.props.addCard(this.props.BoardData[0].id, this.state.input);
      return false;
    }
    return false;
  };

  handleClick = () => {
    this.setState({
      show: this.state.show === true ? this.toggle(this.state.show) : true
    });
  };

  handleChange = e => {
    const input = e.target.value;
    this.setState({
      input
    });
    // console.log(this.state);
  };

  componentDidMount = () => {
    this.props.fetchCard(this.props.id);
  };

  removeCard = id => {
    // console.log(id);
    this.props.removeCard(id);
  };

  render() {
    // console.log(this.props);
    return this.props.lists ? (
      <div className="card">
        {this.props.lists.map(card => {
          return (
            <div className="single-card" key={card.id}>
              <NavLink
                to={{
                  pathname: `/trello/board/${this.props.id}/${card.name}/${
                    card.id
                  }`
                }}
                style={{
                  textDecoration: "none"
                }}
              >
                <Button style={{ width: "70%" }}>
                  <Paper elevation={1} style={{ width: "100%" }}>
                    <Typography
                      component="p"
                      ref={function() {
                        this.inputValue = card;
                      }.bind(this)}
                    >
                      {card.name}
                    </Typography>
                  </Paper>
                </Button>
              </NavLink>
              <Button
                onClick={() => this.removeCard(card.id)}
                style={{ width: "20%", fontSize: "0.8rem" }}
              >
                remove
              </Button>
            </div>
          );
        })}
        {this.state.show && (
          <TextField
            id="standard-textarea"
            placeholder="Card Name"
            multiline
            margin="normal"
            style={{
              padding: "1em",
              fontSize: "1em",
              fontWeight: "500",
              width: "90%"
            }}
            onChange={this.handleChange}
          />
        )}
        <Button style={{ width: "100%" }} onClick={this.handleClick}>
          <Paper
            variant="h4"
            elevation={2}
            style={{
              width: "100%",
              backgroundColor: "black",
              opacity: "0.7"
            }}
          >
            <Typography
              component="h3"
              style={{ textAlign: "center", color: "white", fontWeight: "600" }}
            >
              Add Card
            </Typography>
          </Paper>
        </Button>
      </div>
    ) : (
      <div>loading...</div>
    );
  }
}
const mapStateToProps = state => {
  return {
    lists: state.Reducer.cardData,
    BoardData: state.Reducer.data.lists,
    state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchCard: id => dispatch(fetchCard(id)),
    addCard: (id, name) => dispatch(addCard(id, name)),
    removeCard: id => dispatch(removeCard(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cards);
