import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Cards from "../Cards/Cards";
import { connect } from "react-redux";

class Lists extends Component {
  render() {
    const lists = this.props.BoardData[0];
    // console.log(this.props);

    return (
      <div
        className="List"
        style={{ display: "flex", flexWrap: "wrap", marginLeft: "25%" }}
      >
        <Card style={{ width: "70%" }}>
          <Typography
            style={{ padding: "1em", fontSize: "1em", fontWeight: "500" }}
          >
            {lists.name}
          </Typography>
          <Cards id={lists.id} extraCards={this.props.lists} />
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    BoardData: state.Reducer.data.lists
  };
};

export default connect(
  mapStateToProps,
  null
)(Lists);
