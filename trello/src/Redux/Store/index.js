import { createStore, applyMiddleware, combineReducers } from "redux";
// import thunk from "redux-thunk";
import createSagaMiddleware from "redux-saga";
import Reducer from "../Reducer/Reducer";
import { rootSaga } from "../../Sagas/rootSaga";

const initialState = {};

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  combineReducers({ Reducer }),
  initialState,
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

export default store;
