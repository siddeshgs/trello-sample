import { call, put, takeEvery } from "redux-saga/effects";

import { BOARD, CARD, CHECKLIST, ITEM } from "../Redux/Constants/index";

import axios from "axios";

function* fetchBoardData() {
  try {
    const url =
      "https://api.trello.com/1/boards/5ccfc145477d0c5612028931?actions=all&boardStars=none&cards=none&card_pluginData=false&checklists=none&customFields=false&fields=name%2Cdesc%2CdescData%2Cclosed%2CidOrganization%2Cpinned%2Curl%2CshortUrl%2Cprefs%2ClabelNames&lists=open&members=none&memberships=none&membersInvited=none&membersInvited_fields=all&pluginData=false&organization=false&organization_pluginData=false&myPrefs=false&tags=false&key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e";
    const resp = yield call(axios.get, url);
    const data = resp.data;
    yield put({ type: BOARD.FETCH_BOARD, payload: data });
  } catch (error) {
    console.error(error);
  }
}

function* fetchCardData(actions) {
  try {
    // console.log(actions);
    const id = actions.payload.id;
    const url = `https://api.trello.com/1/lists/${id}/cards?key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`;
    const resp = yield call(axios.get, url);
    const data = resp.data;
    yield put({ type: CARD.FETCH_CARDS, payload: data });
  } catch (error) {
    console.error(error);
  }
}

function* addCard(actions) {
  try {
    const id = actions.payload.id;
    const input = actions.payload.input;
    const url = `https://api.trello.com/1/cards?name=${input}&idList=${id}&keepFromSource=all&key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`;

    const resp = yield call(axios.post, url);
    const data = resp.data;
    yield put({ type: CARD.ADD_CARD, payload: data });
  } catch (error) {
    console.error(error);
  }
}

function* fetchCardChecklist(actions) {
  try {
    const id = actions.payload.id;
    const url = `https://api.trello.com/1/cards/${id}/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`;

    const resp = yield call(axios.get, url);
    const data = resp.data;
    yield put({ type: CARD.FETCH_CHARDCHECKLISTS, payload: data });
  } catch (error) {
    console.error(error);
  }
}

function* addChecklist(actions) {
  try {
    const id = actions.payload.id;
    const name = actions.payload.name;
    const url = `https://api.trello.com/1/cards/${id}/checklists?name=${name}&key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`;

    const resp = yield call(axios.post, url);
    const data = resp.data;
    yield put({ type: CHECKLIST.ADD_CHECKLIST, payload: data });
  } catch (error) {
    console.error(error);
  }
}

function* addItemData(actions) {
  try {
    const data = actions.payload.data;

    yield put({ tyep: ITEM.ADD_ITEM_DATA, payload: data });
  } catch (error) {
    console.error(error);
  }
}

function* addChecklistItem(actions) {
  try {
    const id = actions.payload.id;
    const input = actions.payload.data;
    const url = `https://api.trello.com/1/checklists/${id}/checkItems?name=${input}&pos=bottom&checked=false&key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`;

    const resp = yield call(axios.post, url);
    const data = resp.data;
    yield put({ type: ITEM.ADD_CHECKLIST_ITEM, payload: data });
  } catch (error) {
    console.error(error);
  }
}

function* putChecklistStatus(actions) {
  try {
    const cardID = actions.payload.cardID;
    const itemId = actions.payload.itemId;
    const name = actions.payload.name;
    const state = actions.payload.state;
    const checklistId = actions.payload.checklistId;

    const url = `https://api.trello.com/1/cards/${cardID}/checkItem/${itemId}?name=${name}&state=${state}&idChecklist=${checklistId}&key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`;

    const resp = yield call(axios.put, url);
    const data = resp.data;
    yield put({ type: CHECKLIST.PUT_CHECKLIST_STATUS, payload: data });
  } catch (error) {
    console.error(error);
  }
}

function* removeCard(actions) {
  try {
    const id = actions.payload.id;
    const url = `https://api.trello.com/1/cards/${id}?key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`;

    yield call(axios.delete, url);
    yield put({ type: CARD.REMOVE_CARD, payload: id });
  } catch (error) {
    console.error(error);
  }
}

function* removeChecklist(actions) {
  try {
    const cardId = actions.payload.cardId;
    const checkListId = actions.payload.checkListId;
    const url = `https://api.trello.com/1/cards/${cardId}/checklists/${checkListId}?key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`;
    yield call(axios.delete, url);
    yield put({ type: CHECKLIST.REMOVE_CHECKLIST, payload: checkListId });
  } catch (error) {
    console.error(error);
  }
}

function* removeItem(actions) {
  try {
    const cardId = actions.payload.cardId;
    const itemId = actions.payload.itemId;
    const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`;
    yield call(axios.delete, url);
    yield put({ type: ITEM.REMOVE_ITEM, payload: itemId });
  } catch (error) {
    console.error(error);
  }
}

export function* rootSaga() {
  yield takeEvery(BOARD.LOAD, fetchBoardData);
  yield takeEvery(CARD.LOAD, fetchCardData);
  yield takeEvery(CARD.LOAD_ADD_CARD, addCard);
  yield takeEvery(CARD.LOAD_CARD_CHECKLIST, fetchCardChecklist);
  yield takeEvery(CHECKLIST.LOAD_ADD_CHECKLIST, addChecklist);
  yield takeEvery(ITEM.LOAD_ADD_ITEM_DATA, addItemData);
  yield takeEvery(ITEM.LOAD_ADD_CHECKLIST_ITEM, addChecklistItem);
  yield takeEvery(CHECKLIST.LOAD_PUT_CHECKLIST_STATUS, putChecklistStatus);
  yield takeEvery(CARD.LOAD_REMOVE_CARD, removeCard);
  yield takeEvery(CHECKLIST.LOAD_REMOVE_CHECKLIST, removeChecklist);
  yield takeEvery(ITEM.LOAD_REMOVE_ITEM, removeItem);
}
