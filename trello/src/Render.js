import React, { Component } from "react";
import App from "./App";
import Board from "./Component/Board/Board";
import Checklist from "./Component/Checklist/Checklist";
import { Provider } from "react-redux";
import store from "./Redux/Store/index";
import { BrowserRouter, Route, Switch } from "react-router-dom";

class Render extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <Switch>
            <Route path="/" component={App} exact />
            <Route path="/trello/board/:boardId" component={Board} exact />
            <Route
              path="/trello/board/:listId/:pathParam?/:cardId"
              component={Checklist}
              exact
            />
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default Render;
