import { BOARD, CARD, CHECKLIST, ITEM } from "../Constants/index";

const initialState = {
  listData: [],
  showNewItem: [],
  checklistStatus: [],
  addItemData: [],
  checkbox: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case BOARD.FETCH_BOARD: {
      return {
        ...state,
        data: action.payload
      };
    }
    case CARD.FETCH_CARDS: {
      return {
        ...state,
        cardData: action.payload
      };
    }
    case CARD.ADD_CARD: {
      state.cardData.push(action.payload);
      return {
        ...state,
        cardData: state.cardData
      };
    }
    case CARD.FETCH_CHARDCHECKLISTS: {
      // console.log(action.payload);
      state.showNewItem = action.payload.map(item => {
        return { id: item.id, name: item.name, checklist: item.checkItems };
      });
      state.checkbox = action.payload.map(item => {
        return {
          id: item.id,
          checklists: {
            checklistCheckbox: (function() {
              return item.checkItems.map(itemmy => {
                return {
                  id: itemmy.id,
                  checked: itemmy.state === "complete" ? true : false
                };
              });
            })()
          }
        };
      });
      // state.checklistStatus = action.payload;
      // console.log(state.checkbox);
      return {
        ...state,
        checklistdata: action.payload
      };
    }
    case CHECKLIST.ADD_CHECKLIST: {
      state.checklistdata.push(action.payload);
      return {
        ...state,
        checklistdata: state.checklistdata
      };
    }
    case ITEM.ADD_ITEM_DATA: {
      return {
        ...state,
        addItemData: action.payload
      };
    }
    case ITEM.ADD_CHECKLIST_ITEM: {
      state.checklistdata.map(da => {
        if (da.id === action.payload.idChecklist)
          da.checkItems.push(action.payload);
      });
      return {
        ...state,
        checklistdata: state.checklistdata
      };
    }
    case CHECKLIST.PUT_CHECKLIST_STATUS: {
      state.checklistdata.map(checklist => {
        if (checklist.id === action.payload.idChecklist) {
          checklist.checkItems.map(items => {
            if (action.payload.id === items.id) {
              items.state = action.payload.state;
            }
          });
        }
      });
      return {
        ...state,
        checklistdata: state.checklistdata
      };
    }
    case CARD.REMOVE_CARD: {
      const data = state.cardData;
      // console.log(state.cardData);
      state.cardData = [];
      data.map(checklist => {
        if (action.payload !== checklist.id) state.cardData.push(checklist);
      });
      // console.log(state.cardData);
      return {
        ...state,
        cardData: state.cardData
      };
    }
    case CHECKLIST.REMOVE_CHECKLIST: {
      const data = state.checklistdata;
      state.checklistdata = [];
      data.map(checklist => {
        if (action.payload !== checklist.id)
          state.checklistdata.push(checklist);
      });
      return {
        ...state,
        checklistdata: state.checklistdata
      };
    }
    case ITEM.REMOVE_ITEM: {
      const data = state.checklistdata;
      state.checklistdata = [];
      data.map(checklist => {
        console.log(checklist);
        let miniData = checklist.checkItems;
        checklist.checkItems = [];
        miniData.map(item => {
          if (item.id !== action.payload) checklist.checkItems.push(item);
        });
        state.checklistdata.push(checklist);
      });
      return {
        ...state,
        checklistdata: state.checklistdata
      };
    }
    default:
      return state;
  }
};
